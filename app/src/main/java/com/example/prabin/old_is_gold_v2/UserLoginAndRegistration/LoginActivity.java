package com.example.prabin.old_is_gold_v2.UserLoginAndRegistration;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.prabin.old_is_gold_v2.MainActivity;
import com.example.prabin.old_is_gold_v2.R;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private FirebaseAuth mAuth;
    private EditText mEditEmail, mEditPassword;
    private Button mBtnLogin, mBtnSignup, mBtnReset;

    GoogleSignInClient mGoogleSignInClient;
    SignInButton mGoogleSignIn;

    ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mAuth = FirebaseAuth.getInstance();

        mEditEmail = (EditText) findViewById(R.id.login_editEmail);
        mEditPassword = (EditText) findViewById(R.id.login_editPassword);

        mBtnLogin = (Button) findViewById(R.id.login_btnLogin);
        mBtnSignup = (Button) findViewById(R.id.login_btnSignup);
        mBtnReset = (Button) findViewById(R.id.login_btnReset);

        mProgressBar = (ProgressBar) findViewById(R.id.login_progressBar);
        mProgressBar.setVisibility(View.GONE);

        mBtnLogin.setOnClickListener(this);
        mBtnSignup.setOnClickListener(this);
        mBtnReset.setOnClickListener(this);

        /*findViewById(R.id.btnTest).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FirebaseUser user = mAuth.getCurrentUser();
                if (user != null) {
                    String name = user.getDisplayName();
                    String email = user.getEmail();
                    Uri photoUrl = user.getPhotoUrl();

                    boolean emailVerified = user.isEmailVerified();
                    if (emailVerified) {
                        Toast.makeText(LoginActivity.this, email+" Verified Email", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(LoginActivity.this, email+" Email Has Not Been Verified", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });*/

        mEditPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_DONE) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        //https://firebase.google.com/docs/auth/android/google-signin
        //https://developers.google.com/identity/sign-in/android/sign-in
        // Configure Google Sign In

        mGoogleSignIn = (SignInButton) findViewById(R.id.login_btnGoogleSignin);
        mGoogleSignIn.setSize(SignInButton.SIZE_STANDARD);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        mGoogleSignIn.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {

        //hide keyboard
        //https://stackoverflow.com/questions/1109022/close-hide-the-android-soft-keyboard
        // Check if no view has focus:
        View v = this.getCurrentFocus();
        if (v != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
        }

        switch (view.getId()) {

            case R.id.login_btnLogin:
                attemptLogin();
                break;
            case R.id.login_btnSignup:
                startActivity(new Intent(LoginActivity.this, SignupActivity.class));

                break;
            case R.id.login_btnReset:
                showResetPasswordDialog();
                break;
            case R.id.login_btnGoogleSignin:
                signIn();
                break;
        }
    }

    private void showResetPasswordDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Password Reset!");
        builder.setMessage("Please enter the email");

        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        builder.setView(input);

        builder.setPositiveButton("Send", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                resetPassword();
            }

            private void resetPassword() {
                String email = input.getText().toString();
                mAuth.sendPasswordResetEmail(email)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    Toast.makeText(LoginActivity.this, "Email Sent!", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        builder.show();
    }

    private void attemptLogin() {

        mBtnLogin.setEnabled(false);
        mProgressBar.setVisibility(View.VISIBLE);

        String email = mEditEmail.getText().toString();
        String password = mEditPassword.getText().toString();

        if (TextUtils.isEmpty(email) || TextUtils.isEmpty(password)) {
            mBtnLogin.setEnabled(true);
            mProgressBar.setVisibility(View.GONE);
            return;
        }
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            //Toast.makeText(LoginActivity.this, "Login Successful", Toast.LENGTH_SHORT).show();
                            FirebaseUser user = mAuth.getCurrentUser();
                            if (user != null) {             //user may throw null exception
                                if (user.isEmailVerified()) {
                                    startMainActivity();
                                } else {
                                    showNotConfirmedMessage(user.getEmail());
                                }
                            }

                        } else {
                            Toast.makeText(LoginActivity.this, "Authentication Failed", Toast.LENGTH_SHORT).show();
                        }
                        mBtnLogin.setEnabled(true);
                        mProgressBar.setVisibility(View.GONE);
                    }

                    private void startMainActivity() {
                        startActivity(new Intent(LoginActivity.this, MainActivity.class));
                        finish();
                    }


                    private void showNotConfirmedMessage(String email) {
                        Toast.makeText(LoginActivity.this, email + " has not been verified yet!", Toast.LENGTH_SHORT).show();
                        //Snackbar.make(findViewById(R.id.loginCoordinatorLayout), email + " has not been verified yet!", Snackbar.LENGTH_SHORT).show();
                    }
                });
    }

    private void signIn() {
        mBtnLogin.setEnabled(false);
        mProgressBar.setVisibility(View.VISIBLE);
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, 1234);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1234) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                mBtnLogin.setEnabled(true);
                mProgressBar.setVisibility(View.GONE);                // Google Sign In failed or cancelled
            }
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d("Sign in ID", "firebaseAuthWithGoogle:" + acct.getId());

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(LoginActivity.this, "Signin success", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(LoginActivity.this, MainActivity.class));
                            finish();

                        } else {
                            Toast.makeText(LoginActivity.this, "Authentication Failed", Toast.LENGTH_SHORT).show();
                            //Log.w(TAG, "signInWithCredential:failure", task.getException());
                        }
                        mBtnLogin.setEnabled(true);
                        mProgressBar.setVisibility(View.GONE);
                    }
                });
    }
}
